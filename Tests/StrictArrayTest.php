<?php

namespace StrictArrayTests;

use PHPUnit\Framework\TestCase;
use StrictArray\Exceptions\IncorrectTypeException;
use StrictArray\Exceptions\IncorrectValueTypeException;
use StrictArray\StrictArray;

class StrictArrayTest extends TestCase
{
    public function testTypes()
    {
        $arrayOfInteger = new StrictArray('integer');


        foreach (range(0, 10) as $value) {
            $arrayOfInteger[] = $value;
        }

        $this->assertEquals('integer', $arrayOfInteger->getType());

        $arrayOfDateTime = new StrictArray(\DateTime::class);


        foreach (range(0, 10) as $value) {
            $arrayOfDateTime[] = new \DateTime();
        }

        $this->assertEquals(\DateTime::class, $arrayOfDateTime->getType());

        try {
            $arrayOfDateTime[] = 12;
            $this->assertEquals(true, false);
        } catch (\Throwable $exception) {
            $this->assertInstanceOf(IncorrectValueTypeException::class, $exception);
        }
    }

    public function testParentObjects()
    {
        $arrayOfDateTimeChildren = new StrictArray(\DateTime::class);

        foreach (range(0, 10) as $value) {
            $arrayOfDateTimeChildren[] = new class extends \DateTime
            {
            };

            $this->assertInstanceOf(\DateTime::class, $arrayOfDateTimeChildren[0]);
        }

        try {
            $arrayOfDateTimeChildren[] = new \DateInterval('P2Y4DT6H8M');
            $this->assertEquals(true, false);
        } catch (\Throwable $exception) {
            $this->assertInstanceOf(IncorrectValueTypeException::class, $exception);
        }
    }

    public function testRestrictedTypes()
    {
        try {
            $strictArray = new \StrictArray\StrictArray('');
            $this->assertEquals(true, false);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(IncorrectTypeException::class, $exception);
        }

        try {
            $strictArray = new \StrictArray\StrictArray('  ');
            $this->assertEquals(true, false);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(IncorrectTypeException::class, $exception);
        }

        try {
            $strictArray = new \StrictArray\StrictArray('array');
            $this->assertEquals(true, false);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(IncorrectTypeException::class, $exception);
        }

        try {
            $strictArray = new \StrictArray\StrictArray(StrictArray::class);
            $this->assertEquals(true, false);
        } catch (\Exception $exception) {
            $this->assertInstanceOf(IncorrectTypeException::class, $exception);
        }
    }
}