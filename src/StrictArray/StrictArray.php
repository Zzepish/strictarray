<?php

namespace StrictArray;

use StrictArray\Exceptions\IncorrectTypeException;
use \StrictArray\Exceptions\NullPointerException;
use \StrictArray\Exceptions\IncorrectValueTypeException;

class StrictArray implements \Iterator, \ArrayAccess, \Countable
{
    protected $data = [];
    protected $type;

    protected $pointer;

    protected $generics = [
        'string',
        'integer',
        'boolean',
        'double',
        'null'
    ];

    protected $restricted_types = [
        'array',
        StrictArray::class
    ];

    public function __construct(string $type)
    {
        $type = trim($type);

        $this->type = $type;

        if (!\in_array($type, $this->generics, false)
            &&
            (\in_array($type, $this->restricted_types, false) || !class_exists($type))
        ) {
            throw new IncorrectTypeException(
                'Type "' . $type . '" is restricted! List of restricted types: '
                . implode(', ', $this->restricted_types)
            );
        }

        if (in_array($type, $this->generics)) {
            return;
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function current()
    {
        return current($this->data);
    }

    public function next()
    {
        $next = next($this->data);

        $this->pointer = key($this->data);

        return $next;
    }

    public function key()
    {
        return key($this->data);
    }

    public function valid()
    {
        return array_key_exists($this->pointer, $this->data);
    }

    public function rewind()
    {
        $value         = reset($this->data);
        $this->pointer = key($this->data);

        return $value;
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset)
    {
        if (!array_key_exists($offset, $this->data)) {
            throw new NullPointerException('Array value with key "' . $offset . '" does not exists');
        }

        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if ((!\is_object($value) || !($value instanceof $this->type))
            && (\gettype($value) !== $this->type)) {
            throw new IncorrectValueTypeException('Value type does not match "' . $this->type . '"');
        }

        if (null === $offset) {
            $this->data[] = $value;

            return;
        }

        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        if (!array_key_exists($offset, $this->data)) {
            throw new NullPointerException('Array value with key "' . $offset . '" does not exists');
        }
        unset($this->data[$offset]);

        return $this;
    }

    public function count()
    {
        return \count($this->data);
    }
}
