<?php  
use StrictArray\StrictArray;  
  
$dateTimeCollection = new StrictArray(\DateTime::class);  
$dateTimeCollection[] = new \DateTime();  
  
$integerCollection = new StrictArray('integer');  
$integerCollection[] = 12;  
  
$dateTimeCollection[] = 13 // will throw IncorrectValueTypeException
